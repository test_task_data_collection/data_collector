CREATE TABLE if not exists ad_events.events
(
    client_time Date,
    server_time Date,
    ip          String,
    device_id   UUID,
    device_os   String,
    session     String,
    sequence    Int32,
    event       String,
    param_int   Int32,
    param_str   String
) ENGINE = MergeTree ORDER BY server_time;
