package repo

import (
	"database/sql"
	"fmt"

	"gitlab.com/test_task_data_collection/data_collector/internal/models"
)

type AdEventRepo struct {
	db *sql.DB
}

func NewRepo(db *sql.DB) *AdEventRepo {
	return &AdEventRepo{
		db: db,
	}
}

func (r *AdEventRepo) BatchSaveAdEvents(adEvents []models.EnrichedAdEvent) error {
	const query = `INSERT INTO ad_events.events (client_time, server_time, ip, device_id, device_os, session, sequence, event, param_int, param_str)
VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
`

	stmt, err := r.db.Prepare(query)
	if err != nil {
		return fmt.Errorf("prepare statement: %w", err)
	}
	defer stmt.Close()

	tx, err := r.db.Begin()
	if err != nil {
		return fmt.Errorf("begin tx : %w", err)
	}

	for _, adEvent := range adEvents {
		if _, err = tx.Stmt(stmt).Exec(
			adEvent.ClientTime,
			adEvent.ServerTime,
			adEvent.IP,
			adEvent.DeviceID,
			adEvent.DeviceOS,
			adEvent.Session,
			adEvent.Sequence,
			adEvent.Event,
			adEvent.ParamInt,
			adEvent.ParamStr,
		); err != nil {
			return fmt.Errorf("exec statement: %w", err)
		}
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("commit tx: %w", err)
	}

	return nil
}
