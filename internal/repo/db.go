package repo

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/ClickHouse/clickhouse-go/v2"

	"gitlab.com/test_task_data_collection/data_collector/internal/config"
)

const (
	chDBConnStringEnvName = "CH_DB_CONN_STRING"

	clickhouseDriverName = "clickhouse"

	chConnStringTmpl = "tcp://%v:%v?username=%v&password=%v"
)

func ConnectDB(dbConn string) (*sql.DB, error) {
	db, err := sql.Open(clickhouseDriverName, dbConn)
	if err != nil {
		return nil, fmt.Errorf("open clickhouse connection: %w", err)
	}

	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("ping clickhouse: %w", err)
	}

	return db, nil
}

func PrepareClickhouseDBConnString(cfg config.Clickhouse) string {
	if len(os.Getenv(chDBConnStringEnvName)) > 0 {
		return os.Getenv(chDBConnStringEnvName)
	}
	return fmt.Sprintf(chConnStringTmpl, cfg.Host, cfg.Port, cfg.User, cfg.Password)
}
