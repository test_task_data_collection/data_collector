package bulk_processor

import (
	"context"
	"fmt"
	"sync"
	"time"

	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/config"
	"gitlab.com/test_task_data_collection/data_collector/internal/models"
)

// BulkProcessor should be abstract for production solution
type BulkProcessor interface {
	Add(adEvents []models.EnrichedAdEvent) error
	Flush() (err error)
}

type AdEventsProcessor struct {
	adEvents []models.EnrichedAdEvent
	mu       sync.Mutex

	Cfg    config.BulkProcessor
	Repo   AdEventRepo
	Logger *zap.Logger
}

func (p *AdEventsProcessor) Add(adEvents []models.EnrichedAdEvent) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	//if the number of records exceeds the maximum value then flush
	if len(adEvents)+len(p.adEvents) > p.Cfg.MaxBulkSize {
		go func() {
			if err := p.Flush(); err != nil {
				p.Logger.Error("flush adEvents", zap.Error(err))
			}
		}()
	}

	p.adEvents = append(p.adEvents, adEvents...)

	return nil
}

func (p *AdEventsProcessor) Flush() (err error) {
	p.mu.Lock()
	defer p.mu.Unlock()

	defer func() {
		if err == nil {
			p.adEvents = p.adEvents[:0]
		}
	}()

	if err = p.Repo.BatchSaveAdEvents(p.adEvents); err != nil {
		return fmt.Errorf("upsert coupons: %w", err)
	}

	return nil
}

func (p *AdEventsProcessor) ScheduledFlush(ctx context.Context) {
	requestTicker := time.NewTicker(time.Duration(p.Cfg.Frequency) * time.Second)

	for {
		select {
		case <-requestTicker.C:
			if len(p.adEvents) > 0 {
				go func() {
					if err := p.Flush(); err != nil {
						p.Logger.Error("flush adEvents", zap.Error(err))
					}
				}()
			}
		case <-ctx.Done():
			return
		}
	}
}
