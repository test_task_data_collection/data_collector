package bulk_processor

import "gitlab.com/test_task_data_collection/data_collector/internal/models"

type AdEventRepo interface {
	BatchSaveAdEvents(adEvents []models.EnrichedAdEvent) error
}
