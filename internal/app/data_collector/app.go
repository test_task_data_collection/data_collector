package data_collector

import (
	"context"
	"fmt"

	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/app/data_collector/http/server"
)

type Application struct {
	Logger *zap.Logger
	Server *server.Server
}

func (app *Application) Run(ctx context.Context) {
	if err := app.Server.Run(ctx); err != nil {
		app.Logger.Error("server is down", zap.String("type", "http"), zap.Error(err))
	}
}

func (app *Application) Shutdown() error {
	app.Logger.Info("Shutdown logger")
	if err := app.Logger.Sync(); err != nil {
		return fmt.Errorf("shutdown logger: %w", err)
	}

	app.Logger.Info("Shutdown http server")
	if err := app.Server.Close(); err != nil {
		return fmt.Errorf("shutdown http sever: %w", err)
	}

	return nil
}
