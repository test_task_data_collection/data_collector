package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/valyala/fasthttp"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/models"
)

const (
	ipAddress = "8.8.8.8"
	ok        = "OK"
)

func (s *Server) saveAdEventsHandler() func(ctx *fasthttp.RequestCtx) {
	return func(ctx *fasthttp.RequestCtx) {
		adEvents, err := parseAdEvents(ctx.PostBody())
		if err != nil {
			s.Logger.Error("unmarshal adEvent", zap.Error(err))
			ctx.SetStatusCode(http.StatusBadRequest)
			return
		}

		if err = s.saveAdEvents(adEvents); err != nil {
			s.Logger.Error("saveAdEvents", zap.Error(err))
			ctx.SetStatusCode(http.StatusInternalServerError)
			return
		}

		ctx.SetContentType(ContentText)
		ctx.SetStatusCode(http.StatusOK)
		ctx.SetBody([]byte(ok))
	}
}

func parseAdEvents(body []byte) ([]models.AdEvent, error) {
	bEvents := bytes.Split(bytes.Trim(body, "\n"), []byte("\n"))

	var adEvents []models.AdEvent
	for _, bEvent := range bEvents {
		if len(bEvent) == 0 {
			continue
		}
		var adEvent models.AdEvent
		if err := json.Unmarshal(bEvent, &adEvent); err != nil {
			return nil, fmt.Errorf("unmarshal adEvent: %w", err)
		}
		adEvents = append(adEvents, adEvent)
	}

	return adEvents, nil
}

func (s *Server) saveAdEvents(adEvents []models.AdEvent) error {
	now := time.Now()

	enrichedAdEvents := make([]models.EnrichedAdEvent, 0, len(adEvents))
	for _, event := range adEvents {
		enrichedAdEvents = append(enrichedAdEvents, models.EnrichedAdEvent{
			AdEvent:    event,
			IP:         ipAddress,
			ServerTime: now,
		})
	}

	if err := s.BulkProcessor.Add(enrichedAdEvents); err != nil {
		return fmt.Errorf("bulkProcessor.Add: %w", err)
	}

	return nil
}
