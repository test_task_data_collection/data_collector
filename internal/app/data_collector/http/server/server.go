package server

import (
	"context"
	"fmt"
	"net"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/bulk_processor"
	"gitlab.com/test_task_data_collection/data_collector/internal/config"
)

const (
	AcceptJson  = "application/json"
	ContentText = "text/plain; charset=utf8"
	ContentJson = "application/json; charset=utf-8"
)

type Server struct {
	Logger *zap.Logger
	Config config.HTTPServer

	Ln     net.Listener
	Router *router.Router

	BulkProcessor bulk_processor.BulkProcessor
}

func (s *Server) Close() error {
	if err := s.Ln.Close(); err != nil {
		return fmt.Errorf("close listener: %w", err)
	}
	return nil
}

func (s *Server) Run(ctx context.Context) error {
	s.muxRouter()

	srv := &fasthttp.Server{
		Handler:            s.Router.Handler,
		Name:               s.Config.ServerSettings.Name,
		MaxConnsPerIP:      s.Config.ServerSettings.MaxConnsPerIP,
		MaxRequestsPerConn: s.Config.ServerSettings.MaxRequestsPerConn,
		Concurrency:        s.Config.ServerSettings.Concurrency,
	}

	return srv.Serve(s.Ln)
}
