package models

import "time"

type AdEvent struct {
	ClientTime time.Time `json:"client_time"`
	DeviceID   string    `json:"device_id"`
	DeviceOS   string    `json:"device_os"`
	Session    string    `json:"session"`
	Event      string    `json:"event"`
	ParamStr   string    `json:"param_str"`
	Sequence   int32     `json:"sequence"`
	ParamInt   int32     `json:"param_int"`
}

type EnrichedAdEvent struct {
	AdEvent
	ServerTime time.Time `json:"server_time"`
	IP         string    `json:"ip"`
}
