package config

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

const cfgFilePathTemplate = "%s/configs/%s.yml"

type Config struct {
	HTTPServer    HTTPServer    `mapstructure:"http_server"`
	Clickhouse    Clickhouse    `mapstructure:"clickhouse"`
	BulkProcessor BulkProcessor `mapstructure:"bulk_processor"`
}

type BulkProcessor struct {
	Frequency   int `mapstructure:"frequency"`
	MaxBulkSize int `mapstructure:"max_bulk_size"`
}

type HTTPServer struct {
	Address        string         `mapstructure:"address"`
	ServerSettings ServerSettings `mapstructure:"server_settings"`
}

// ServerSettings configuration for fasthttp
type ServerSettings struct {
	// Server name for sending in response headers.
	//
	// Default server name is used if left blank.
	Name string `mapstructure:"name"`

	// The maximum number of concurrent connections the server may serve.
	//
	// DefaultConcurrency is used if not set.
	Concurrency int `mapstructure:"concurrency"`

	// Maximum number of concurrent client connections allowed per IP.
	//
	// By default unlimited number of concurrent connections
	// may be established to the server from a single IP address.
	MaxConnsPerIP int `mapstructure:"max_conns_per_ip"`

	// Maximum number of requests served per connection.
	//
	// The server closes connection after the last request.
	// 'Connection: close' header is added to the last response.
	//
	// By default unlimited number of requests may be served per connection.
	MaxRequestsPerConn int `mapstructure:"max_requests_per_conn"`
}

type Clickhouse struct {
	Host     string `mapstructure:"host"`
	Port     string `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
}

func NewConfig(configFilePath string) (*Config, error) {
	config, err := loadConfig(configFilePath)
	if err != nil {
		return nil, fmt.Errorf("loadConfig: %w", err)
	}

	return config, nil
}

func loadConfig(configFilePath string) (*Config, error) {
	viper.SetConfigFile(configFilePath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, fmt.Errorf("readInConfig: %w", err)
	}

	var appConfig Config
	if err := viper.Unmarshal(&appConfig); err != nil {
		return nil, fmt.Errorf("unmarshal config: %w", err)
	}

	return &appConfig, nil
}

func PrepareCfgFilePath(env, currentPath, appPathEnvName string) string {
	appPath := currentPath
	if len(os.Getenv(appPathEnvName)) > 0 {
		appPath = os.Getenv(appPathEnvName)
	}

	return fmt.Sprintf(cfgFilePathTemplate, appPath, env)
}
