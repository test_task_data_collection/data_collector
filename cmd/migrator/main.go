package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"

	_ "github.com/ClickHouse/clickhouse-go"
	"github.com/golang-migrate/migrate"
	clickhouseMigrate "github.com/golang-migrate/migrate/database/clickhouse"
	_ "github.com/golang-migrate/migrate/source/file"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/config"
	ll "gitlab.com/test_task_data_collection/data_collector/internal/logger"
	"gitlab.com/test_task_data_collection/data_collector/internal/repo"
)

const (
	migratorName = "migrator"

	appPathEnvName = "APP_PATH"

	currentPath = "."

	clickhouseDriverName = "clickhouse"

	infoLogLevel = "info"
)

func main() {
	var sourceUrl, env string
	flag.StringVar(&env, "e", "", "environment")
	flag.StringVar(&sourceUrl, "m", "", "migrations source")
	flag.Parse()

	appConfig, err := config.NewConfig(config.PrepareCfgFilePath(env, currentPath, appPathEnvName))
	if err != nil {
		log.Fatal("error while read config", err.Error())
	}

	logger, err := ll.New(migratorName, env, infoLogLevel)
	if err != nil {
		log.Fatal("error while init logger", err.Error())
	}

	migrateClickhouse(logger, appConfig.Clickhouse, sourceUrl)

	logger.Info("migrator finished")
}

func migrateClickhouse(logger *zap.Logger, cfg config.Clickhouse, sourceUrl string) {
	db, err := sql.Open(clickhouseDriverName, repo.PrepareClickhouseDBConnString(cfg))
	if err != nil {
		logger.Error("open clickhouse sql conn", zap.Error(err))
		return
	}

	defer db.Close()
	driver, err := clickhouseMigrate.WithInstance(db, &clickhouseMigrate.Config{
		MultiStatementEnabled: true,
	})
	if err != nil {
		logger.Error("create clickhouse db instance", zap.Error(err))
		return
	}

	m, err := migrate.NewWithDatabaseInstance(fmt.Sprint("file://", sourceUrl, "/", clickhouseDriverName), clickhouseDriverName, driver)
	if err != nil {
		logger.Error("create clickhouse migrate instance", zap.Error(err))
		return
	}

	if err = m.Up(); err != nil {
		if err == migrate.ErrNoChange {
			logger.Info("there are no new clickhouse migrations")
			return
		}
		logger.Error("up clickhouse migrations", zap.Error(err))
		return
	}
	logger.Info("new clickhouse migrations applied")
}
