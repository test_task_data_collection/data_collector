package main

import (
	"context"
	"flag"
	"log"
	"os/signal"
	"syscall"
	"time"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp/reuseport"
	"go.uber.org/zap"

	"gitlab.com/test_task_data_collection/data_collector/internal/app/data_collector"
	"gitlab.com/test_task_data_collection/data_collector/internal/app/data_collector/http/server"
	"gitlab.com/test_task_data_collection/data_collector/internal/bulk_processor"
	"gitlab.com/test_task_data_collection/data_collector/internal/config"
	ll "gitlab.com/test_task_data_collection/data_collector/internal/logger"
	"gitlab.com/test_task_data_collection/data_collector/internal/repo"
)

const (
	serviceName = "data_collector"

	appPathEnvName = "APP_PATH"

	currentPath = "."

	envFlagName         = "e"
	envFlagDefault      = "local"
	logLevelFlagName    = "ll"
	logLevelFlagDefault = "local"

	networkTCP4 = "tcp4"
)

func main() {
	// Create context that listens for the interrupt signal from the OS.
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	var env, logLevel string
	flag.StringVar(&env, envFlagName, envFlagDefault, "environment")
	flag.StringVar(&logLevel, logLevelFlagName, logLevelFlagDefault, "logging level")
	flag.Parse()

	cfg, err := config.NewConfig(config.PrepareCfgFilePath(env, currentPath, appPathEnvName))
	if err != nil {
		log.Fatal("error while reading config", err.Error())
	}

	chClient, err := repo.ConnectDB(repo.PrepareClickhouseDBConnString(cfg.Clickhouse))
	if err != nil {
		log.Fatal("error while connect to ch database", zap.Error(err))
	}
	defer chClient.Close()

	logger, err := ll.New(serviceName, env, logLevel)
	if err != nil {
		log.Println("error while initing logger", err.Error())
	}

	listener, err := reuseport.Listen(networkTCP4, cfg.HTTPServer.Address)
	if err != nil {
		logger.Error("listen", zap.String("address", cfg.HTTPServer.Address), zap.Error(err))
	}

	bulkProcessor := &bulk_processor.AdEventsProcessor{
		Cfg:    cfg.BulkProcessor,
		Repo:   repo.NewRepo(chClient),
		Logger: logger,
	}

	app := &data_collector.Application{
		Logger: logger,
		Server: &server.Server{
			Logger:        logger,
			Config:        cfg.HTTPServer,
			Router:        router.New(),
			Ln:            listener,
			BulkProcessor: bulkProcessor,
		},
	}

	go app.Run(ctx)
	go bulkProcessor.ScheduledFlush(ctx)

	// Listen for the interrupt signal.
	<-ctx.Done()

	// Restore default behavior on the interrupt signal and notify user of shutdown.
	stop()
	log.Println("shutting down gracefully")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	_, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err = app.Shutdown(); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting")
}
