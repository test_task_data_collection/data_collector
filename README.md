# Data Collector

Date Collector is an application for processing and enriching advertising events.

At current settings, the application can process about 1000 requests per second, running on my local machine with 8 virtual cores and 16 GB of RAM. I am sure that if spend more time on the selection of optimal configuration values, RPS can exceed 1000.

The application is automatically deployed on a virtual server and can be accessed at this address: `http://45.147.248.142:3080`
Unfortunately, the server is rather weak in its characteristics and high RPS cannot be achieved there (it can achieve more than 200 RPS but server memory is very limited).

In my opinion, the application cannot be called "ready for use in production", but it is suitable as a result of a test task. Here are some things I would like to do first:
* write unit tests (first of all for `saveAdEventsHandler` handler)
* remove sensitive data from the repository
* if the application is going to expand, I would add a separate abstraction layer for handlers. At the moment, the `type Server` carries functionality that should not carry.
* I would make the `BulkProcessor` interface more abstract so that it can be applied to any entity and not only to `[]models.EnrichedAdEvent`
* (depends on business needs) I would add a message broker to the system as an intermediary between the server and the database. This would guarantee additional system fault tolerance and increase server throughput by removing costly database communication operations from it.

### Available methods
`POST /ad_events`
