# Common
NAME = data_collector
MIGRATOR_NAME = migrator
GO_PACKAGE = gitlab.com/test_task_data_collection/data_collector

#.PHONY: gen
#gen:
#	go install github.com/swaggo/swag/cmd/swag@v1.8.1
#	swag init --parseDependency --parseInternal --parseDepth 3 --md docs -g cmd/self_service/main.go

# Build
BUILD_CMD ?= CGO_ENABLED=0 go build -o bin/${NAME} -ldflags '-v -w -s' ${GO_PACKAGE}/cmd/${NAME}
DEBUG_CMD ?= CGO_ENABLED=0 go build -o bin/${NAME} -gcflags "all=-N -l" ${GO_PACKAGE}/cmd/${NAME}
BUILD_MIGRATOR_CMD ?= go build -o bin/${MIGRATOR_NAME} ${GO_PACKAGE}/cmd/${MIGRATOR_NAME}

.PHONY: check
check:
	go vet ./...
	go install -v github.com/golangci/golangci-lint/cmd/golangci-lint@v1.51.1
	golangci-lint run

.PHONY: tests
tests:
	go test -v -race ./...

.PHONY: precompile
precompile:
	go build ./...

.PHONY: clean
clean:
	@echo "> Cleaning binaries for ${NAME}"
	@-rm -rf bin/${NAME}

.PHONY: build
build: clean
	@echo "> Build ${NAME}"
	@${BUILD_CMD}

.PHONY: build_debug
build_debug: clean
	@echo "> Build ${NAME}"
	@${DEBUG_CMD}

.PHONY: clean_migrator
clean_migrator:
	@echo "> Cleaning binaries for ${MIGRATOR_NAME}"
	@-rm -rf bin/${MIGRATOR_NAME}

.PHONY: build_migrator
build_migrator: clean_migrator
	@echo "> Build migrator"
	@${BUILD_MIGRATOR_CMD}
